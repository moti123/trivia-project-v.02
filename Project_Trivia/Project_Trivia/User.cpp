#include "User.h"

User::User(string username, SOCKET sock)
{
	_userName = username;
	_sock = sock;
	_currRoom = nullptr;
	_currGame = nullptr;
}
void User::send(string message)
{
	Helper::sendData(this->_sock, message);
}
void User::setGame(Game* gm)
{	
	_currRoom = nullptr;
	_currGame = gm;
}
void User::clearRoom()
{
	_currGame = nullptr;
	_currRoom = nullptr;
}
bool User:: createRoom(int roomId, string roomName, int maxUsers, int questionsNum, int questionTime)
{
	if (this->_currRoom == nullptr)
	{
		Room *newRoom = new Room(roomId, this, roomName, maxUsers, questionsNum, questionTime);
		this->_currRoom = newRoom;
		this->send(CREATE_ROOM_SUCCESS_ANS);
		return true;
	}
	else
	{
		this->send(CREATE_ROOM_FAIL_ANS);
		return false;
	}
}
bool User::joinRoom(Room* newRoom)
{
	if (this->_currRoom == nullptr)
	{
		if (newRoom->joinRoom(this))
		{
			this->_currRoom = newRoom;
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return FALSE;
	}
}
void User::leaveRoom()
{
	if (this->_currRoom != nullptr)
	{
		this->_currRoom->leaveRoom(this);
		this->_currRoom = nullptr;
	}
};
int User::closeRoom()
{
	if (this->_currRoom != nullptr)
	{
		int roomId = this->_currRoom->closeRoom(this);
		if (roomId == -1)
		{
			return -1;
		}
		else
		{
			delete(this->_currRoom);
			return (roomId);
		}
	}
	else
	{
		return -1;
	}
}
bool User::leaveGame()
{
	if (this->_currGame != nullptr)
	{
		bool retVal = this->_currGame->leaveGame(this);
		this->_currGame = nullptr;
		return retVal;
	}
	else
	{
		return FALSE;
	}
}
string User::getUserName()
{
	return _userName;
}
SOCKET User::getSocket()
{
	return this->_sock;
}
Room* User::getRoom()
{
	return this->_currRoom;
}
Game* User::getGame()
{
	return this->_currGame;
}



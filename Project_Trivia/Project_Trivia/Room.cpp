#include "Room.h"

Room::Room(int id, User* admin, string name, int maxUsers, int questionsNo, int questionTime)
{
	_id = id;
	_name = name;
	_maxUsers = maxUsers;
	_questionNo = questionsNo;
	_questionTime = questionTime;
	_admin = admin;
	_users.push_back(admin);
};
string Room:: getUsersListMessage()
{
	ostringstream convert;
	convert << this->_users.size();
	string str = USER_ROOM_LIST_ANS + convert.str();

	for (unsigned int i = 0; i < this->_users.size(); i++)
	{
		ostringstream convert2;
		if (_users[i]->getUserName().length() < 10)
		{
			str += "0";
			convert2 << _users[i]->getUserName().length();
			str += convert2.str();
			str += _users[i]->getUserName();
		}
		else
		{
			convert2 << _users[i]->getUserName().length();
			str += convert2.str();
			str += _users[i]->getUserName();
		}
	}

	return str;
}
bool Room:: joinRoom(User* user)
{
	if (_users.size() == _maxUsers)
	{
		user->send(JOIN_ROOM_FAIL_FULL_ANS);
		return FALSE;
	}
	else
	{
		_users.push_back(user);
		ostringstream convert;
		convert << _questionNo;
		string str = JOIN_ROOM_SUCCESS_ANS;
		if (_questionNo < 10)
		{
			str += "0";
		}
		str += convert.str();
		ostringstream convert2;
		convert2 << _questionTime;
		if (_questionTime < 10)
		{
			str += "0";
		}
		str += convert2.str();
		user->send(str);
		sendMessage(getUsersListMessage());

		return TRUE;
	}
}
void Room::leaveRoom(User* user)
{
	for (unsigned int i = 0; i < _users.size(); i++)
	{
		if (user->getUserName() == _users[i]->getUserName())
		{
			_users.erase(_users.begin() + i);
			user->send(LEAVE_ROOM_SUCCESS_ANS);
			sendMessage(getUsersListMessage());
		}
	}
}
int Room::closeRoom(User* user)
{
	if (user->getUserName() == _admin->getUserName())
	{
		sendMessage(CLOSE_ROOM_ANS);
		for (unsigned int i = 0; i < this->_users.size(); i++)
		{
			if (this->_users[i]->getUserName() != this->_admin->getUserName())
			{
				this->_users[i]->clearRoom();
			}
		}
		return (this->_id);
	}
	else
	{
		return -1;
	}
}
vector<User*> Room:: getUsers()
{
	return (this->_users);
};
int Room:: getQuestionsNo()
{
	return (this->_questionNo);
};
int Room:: getId()
{
	return (this->_id);
}

int Room::getQuestionTime()
{
	return (this->_questionTime);
}

string Room:: getName()
{
	return (this->_name);
}


string Room::getUsersAsString(vector<User*> _users, User* excludeUser)
{
	string str = "";
	for (unsigned int i = 0; i < _users.size(); i++)
	{
		if (_users[i]->getUserName() != excludeUser->getUserName())
		{
			str += _users[i]->getUserName();
			str += ", ";
		}
	}
	return str;
}
void Room ::sendMessage(User* excludeUser, string message)
{
	for (unsigned int i = 0; i < this->_users.size(); i++)
	{
		if (this->_users[i] != excludeUser)
		{
			this->_users[i]->send(message);
		}
	}
}
void Room ::sendMessage(string message)
{
	sendMessage(NULL, message);
}

	
#include "RecievedMessage.h"

RecievedMessage::RecievedMessage(SOCKET s, User* user, int messageCode, vector<string> values)
{
	_s = s;
	_user = user;
	_messageCode = messageCode;
	_values = values;

}

RecievedMessage::RecievedMessage(SOCKET s, User* user, int messageCode)
{
	_s = s;
	_user = user;
	_messageCode = messageCode;
}

RecievedMessage::~RecievedMessage(){}

User* RecievedMessage::getUser()
{
	return this->_user;
}

SOCKET RecievedMessage::getSocket()
{
	return this->_s;
}

vector<string> RecievedMessage::getValues()
{
	return this->_values;
}

int RecievedMessage::getMessageCode()
{
	return this->_messageCode;
}

#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "Question.h"
#include "sqlite3.h"

using namespace std;

static sqlite3*_db;

class DataBase
{
public:
	DataBase();
	~DataBase();

	bool isUserExits(string username);
	bool addNewUser(string username, string password, string email);
	bool isUserAndPassMatch(string username, string password);

	vector<Question*> initQuestions(int questionsNo);

	int insertNewGame();
	bool updateGameStatus(int gameId);
	bool addAnswerToPlayer(int gameId, string username, int questionId, string answer, bool isCorrect, int answerTime);

private:
	vector<Question*> _vec;
	char *sql;
	char *zErrMsg = 0;
};


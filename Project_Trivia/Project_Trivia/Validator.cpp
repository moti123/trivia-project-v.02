#include "Validator.h"

Validator::Validator(){}
Validator::~Validator(){}

bool Validator::isPasswordValid(string pass)
{
	int countDigits = 0, countUpper = 0, countLower = 0;
	if (pass.size() >= 4)
	{
		for (unsigned int i = 0; i < pass.size(); i++)
		{
			if (pass[i] == 32)
			{
				return false;
			}
		}
		for (unsigned int i = 0; i < pass.size(); i++)
		{
			if (pass[i] >= '0' && pass[i] <= '9')
			{
				countDigits++;
			}
			else if (pass[i] >= 'A' && pass[i] <= 'Z')//Upper case
			{
				countUpper++;
			}
			else if (pass[i] >= 'a' && pass[i] <= 'z')//Lower case
			{
				countLower++;
			}
		}
		if (countDigits < 1 || countLower < 1 || countUpper < 1)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	return false;
}
bool Validator::isUserNameValid(string userName)
{
	int i = 0;
	if (userName.size() > 0)
	{
		if ((userName[0] >= 'a' && userName[0] <= 'z') || (userName[0] >= 'A' && userName[0] <= 'Z'))
		{
			for (unsigned int i = 0; i < userName.size(); i++)
			{
				if (userName[i] == 32)
				{
					return false;
				}
			}
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}
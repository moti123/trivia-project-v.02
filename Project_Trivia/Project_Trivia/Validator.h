#pragma once
#include <iostream>
#include <string>

using namespace std;

class Validator
{
public:
	Validator();
	~Validator();

	static bool isPasswordValid(string pass);
	static bool isUserNameValid(string userName);

};
#pragma once
#include <sstream>
#include "User.h"
#include <vector>
#include "protocol.h"

using namespace std;

class User;
class Room
{
public:
	Room(int id, User* admin, string name, int maxUsers, int questionsNo, int questionTime);
	string getUsersListMessage();
	bool joinRoom(User* user);
	void leaveRoom(User* user);
	int closeRoom(User* user);
	vector<User*> getUsers();
	int getQuestionsNo();
	int getId();
	int getQuestionTime();
	string getName();

private:
	string getUsersAsString(vector<User*> _users, User* excludeUser);
	void sendMessage(User* excludeUser, string message);
	void sendMessage(string message);

	vector<User*> _users;
	User* _admin;
	int _maxUsers;
	int _questionTime;
	int _questionNo;
	string _name;
	int _id;
};
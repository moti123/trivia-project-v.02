#pragma once
#include <iostream>
#include <string>
#include "User.h"


using namespace std;

class RecievedMessage
{
public:
	RecievedMessage(SOCKET s, User* user, int messageCode, vector<string> values);
	RecievedMessage(SOCKET s, User*, int messageCode);
	~RecievedMessage();

	User* getUser();
	SOCKET getSocket();
	vector<string> getValues();
	int getMessageCode();


private:
	User * _user;
	SOCKET _s;
	int _messageCode;
	vector<string>_values;
};
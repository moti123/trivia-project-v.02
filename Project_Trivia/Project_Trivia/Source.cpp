#include <thread>
#include <deque>
#include <map>
#include <condition_variable>
#include <WinSock2.h>
#include <Windows.h>
#include "DataBase.h"
#include "sqlite3.h"
#include "TriviaServer.h"


using namespace std;

void main()
{
	try
	{

		TRACE("Starting...");
		TriviaServer* server = new TriviaServer();
		server->serve();
	}
	catch (const std::exception& e)
	{
		std::cout << "Exception was catch in function: " << e.what() << std::endl;
	}
	catch (...)
	{
		std::cout << "Unknown exception in main !" << std::endl;
	}
	system("PAUSE");
}

#include "Question.h"

Question::Question(int id, string question, string correctAnswer, string answer2, string answer3, string answer4)
{
	this->_id = id;
	this->_question = question;
	//srand(time(NULL));
	
	int r = rand() % 4;

	switch (r)
	{
	case 0:
		this->_answers[0] = correctAnswer;
		this->_answers[1] = answer2;
		this->_answers[2] = answer3;
		this->_answers[3] = answer4;
		
		break;
	case 1:
		this->_answers[1] = correctAnswer;
		this->_answers[0] = answer2;
		this->_answers[2] = answer3;
		this->_answers[3] = answer4;

		break;
	case 2:
		this->_answers[2] = correctAnswer;
		this->_answers[1] = answer2;
		this->_answers[0] = answer3;
		this->_answers[3] = answer4;

		break;
	case 3:
		this->_answers[3] = correctAnswer;
		this->_answers[1] = answer2;
		this->_answers[2] = answer3;
		this->_answers[0] = answer4;
		
		break;
	}

	
	this->_correctAnswerIndex = r;
}
string Question::getQuestion()
{
	return (this->_question);
}
string* Question::getAnswers()
{
	return (this->_answers);

}
int Question::getCorrectAnsewrIndex()
{
	return this->_correctAnswerIndex;
}
int Question::getID()
{
	return this->_id;
}
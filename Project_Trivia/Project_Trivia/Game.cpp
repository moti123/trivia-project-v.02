#include "Game.h"

Game::Game(const vector<User*>& players, int questionsNo, DataBase& db, int id)
{
	_db = db;
	this->_id = id;
	//_db.insertNewGame();
	//_questions = _db.initQuestions(questionsNo);
	_players = players;
	this->_questions_no = questionsNo;

	for (unsigned int i = 0; i < this->_players.size(); i++)
	{
		_results[_players[i]->getUserName()] = 0;
	}

	for (unsigned int i = 0; i < this->_players.size(); i++)
	{
		this->_players[i]->setGame(this);
	}
	this->_currQuestionIndex = 0;

	string theQuestion, ans1, ans2, ans3, ans4;
	ifstream myfile;
	myfile.open("questions.txt");

	if (myfile.is_open())
	{
		for (int i = 0; i < questionsNo; i++)
		{
			getline(myfile, theQuestion);
			getline(myfile, ans1);
			getline(myfile, ans2);
			getline(myfile, ans3);
			getline(myfile, ans4);
			Question *question = new Question(i, theQuestion, ans1, ans2, ans3, ans4);
			this->_questions.push_back(question);
		}
		myfile.close();
	}

	else cout << "Unable to open file";
}
Game::~Game()
{
	this->_questions.clear();
	this->_players.clear();
}

void Game::sendQuestionToAllUsers()
{
	string str = "";
	str += BEGIN_GAME_ANS;

	Question *currQuestion;
	currQuestion = this->_questions[this->_currQuestionIndex];

	ostringstream convert;
	convert << currQuestion->getQuestion().size();

	if (currQuestion->getQuestion().size() < 10)
	{
		str += "00";
		str += convert.str();
	}
	else if (currQuestion->getQuestion().size() < 100)
	{
		str += "0";
		str += convert.str();
	}
	else
	{
		str += convert.str();
	}

	str += currQuestion->getQuestion();

	for (int i = 0; i < 4; i++)
	{
		ostringstream convert2;
		convert2 << currQuestion->getAnswers()[i].size();

		if (currQuestion->getAnswers()[i].size() < 10)
		{
			str += "00";
			str += convert2.str();
		}
		else if (currQuestion->getAnswers()[i].size() < 100)
		{
			str += "0";
			str += convert2.str();
		}
		else
		{
			str += convert2.str();
		}

		str += currQuestion->getAnswers()[i];
	}

	this->_currentTurnAnswers = 0;

	try
	{
		for (unsigned int i = 0; i < (this->_players.size()); i++)
		{
			this->_players[i]->send(str);
		}
	}
	catch (...)
	{

	}
}
void Game::handleFinishGame()
{
	string str = "";
	ostringstream convert;
	str += SERVER_END_GAME_ANS;
	convert << this->_players.size();
	str += convert.str();

	for (unsigned int i = 0; i < this->_players.size(); i++)
	{
		ostringstream convert2;

		if (this->_players[i]->getUserName().size() < 10)
		{
			str += "0";
		}

		convert2 << this->_players[i]->getUserName().size();
		str += convert2.str();
		str += this->_players[i]->getUserName();

		ostringstream convert3;

		map<std::string, int>::iterator it;
		it = this->_results.find(this->_players[i]->getUserName());
		if (it != this->_results.end())
		{
			convert3 << it->second;
			if (it->second < 10)
			{
				str += "0";
			}
			str += convert3.str();
		}
	}

	for (unsigned int i = 0; i < this->_players.size(); i++)
	{
		this->_players[i]->send(str);
		this->_players[i]->setGame(nullptr);
	}

}
void Game::sendFirstQuestion()
{
	sendQuestionToAllUsers();
	this->_currQuestionIndex++;
}
bool Game::handleNextTurn()
{
	if (this->_players.size() == 0)
	{
		handleFinishGame();
		return FALSE;
	}
	else if (this->_players.size() == this->_currentTurnAnswers)
	{
		if (this->_currQuestionIndex == this->_questions_no)
		{
			handleFinishGame();
			return FALSE;
		}
		else
		{
			sendQuestionToAllUsers();
			this->_currQuestionIndex++;

			return TRUE;
		}
	}
	else
	{
		return TRUE;
	}
}
bool Game::handleAnswerFromUser(User* user, int answerNo, int time)
{

	this->_currentTurnAnswers++;
	string str = "120";

	if (answerNo == this->_questions[this->_currQuestionIndex - 1]->getCorrectAnsewrIndex() + 1)
	{
		this->_results[user->getUserName()]++;
		//_db.addAnswerToPlayer(this->getID(), user->getUserName(), this->_questions[_currQuestionIndex]->getID(), this->_questions[_currQuestionIndex]->getAnswers()[answerNo], TRUE, time);
		str += "1";
		user->send(str);
	}
	else if (answerNo == 4)
	{
		//_db.addAnswerToPlayer(this->getID(), user->getUserName(), this->_questions[_currQuestionIndex]->getID(), NULL, FALSE, time);
		str += "0";
		user->send(str);
	}
	else
	{
		//_db.addAnswerToPlayer(this->getID(), user->getUserName(), this->_questions[_currQuestionIndex]->getID(), this->_questions[_currQuestionIndex]->getAnswers()[answerNo], FALSE, time);
		str += "0";
		user->send(str);
	}

	return handleNextTurn();

}
bool Game::leaveGame(User* currUser)
{
	for (unsigned int i = 0; i < (this->_players.size()); i++)
	{
		if (this->_players[i]->getUserName() == currUser->getUserName())
		{
			_players.erase(_players.begin() + i);
		}
	}

	return handleNextTurn();
}

int Game::getID()
{
	return this->_id;
}
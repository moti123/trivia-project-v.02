#include "TriviaServer.h"

TriviaServer::TriviaServer()//create server
{
	struct addrinfo hints;
	struct addrinfo *result = NULL;

	srand((unsigned int)time(NULL));
	WSADATA wsa_data = {};
	if (WSAStartup(MAKEWORD(2, 2), &wsa_data) != 0)
		throw exception("WSAStartup fail");

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;
	int iResult = getaddrinfo(NULL, PORT, &hints, &result);
	if (iResult != 0)
	{
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
	}
	_s = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (_s == INVALID_SOCKET)
	{
		printf("socket failed with error: %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
	}
	iResult = ::bind(_s, result->ai_addr, (int)result->ai_addrlen);

	if (iResult == SOCKET_ERROR) {
		printf("bind failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(_s);
		WSACleanup();

		freeaddrinfo(result);
	}
}
TriviaServer::~TriviaServer()//d'tor server
{
	map<SOCKET, User*>::iterator it1;
	map<int, Room*>::iterator it2;
	for (it1 = _connectedUsers.begin(); it1 != _connectedUsers.end(); ++it1)
	{
		_connectedUsers.erase(it1);
	}
	for (it2 = _roomsList.begin(); it2 != _roomsList.end(); ++it2)
	{
		_roomsList.erase(it2);
	}
	TRACE(__FUNCTION__ " closing accepting socket");
	try
	{
		::closesocket(_s);
	}
	catch (...) {}
}

void TriviaServer::serve()//open lop of listenning
{
	bindAndListen();
	while (true)
	{
		TRACE("accepting client...");
		accept();
	}
}
void TriviaServer::bindAndListen()//listenning
{
	std::thread tr1(&TriviaServer::handleRecievedMessages, this);
	int iResult = listen(_s, SOMAXCONN);
	if (iResult == SOCKET_ERROR)
	{
		printf("listen failed with error: %d\n", WSAGetLastError());
		closesocket(_s);
		WSACleanup();
	}
	tr1.detach();
}
void TriviaServer::accept()//accept clients
{
	SOCKET client_socket = ::accept(_s, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " -accept");

	TRACE("Client accepted !");
	// create new thread for client	and detach from it
	std::thread tr2(&TriviaServer::clientHandler, this, client_socket);
	tr2.detach();
}

void TriviaServer::clientHandler(SOCKET client_socket)//need to look on this
{
	int messageTypeCode = helper.getMessageTypeCode(client_socket);

	while (messageTypeCode != 0 && messageTypeCode != LEAVE_APP)
	{
		try
		{
			msg = buildRecieveMessage(client_socket, messageTypeCode);
			addRecievedMessage(msg);

		}
		catch (...)
		{
			RecievedMessage * msg = buildRecieveMessage(client_socket, LEAVE_APP);
			addRecievedMessage(msg);
		}
		messageTypeCode = helper.getMessageTypeCode(client_socket);
	}

}

Room* TriviaServer::getRoomById(int roomId)
{
	map<int, Room*>::iterator it;
	for (it = _roomsList.begin(); it != _roomsList.end(); it++)
	{
		if (it->first == roomId)
		{
			return it->second;
		}
	}
	return NULL;
}
User* TriviaServer::getUserByName(string userName)
{
	map<SOCKET, User*>::iterator it;
	for (it = _connectedUsers.begin(); it != _connectedUsers.end(); it++)
	{
		if (it->second->getUserName() == userName)
		{
			return it->second;
		}
	}
	return NULL;

}
User* TriviaServer::getUserBySocket(SOCKET client_socket)
{
	map<SOCKET, User*>::iterator it;
	for (it = _connectedUsers.begin(); it != _connectedUsers.end(); it++)
	{
		if (it->first == client_socket)
		{
			return it->second;
		}
	}
	return NULL;
}

void TriviaServer::handleRecievedMessages()
{
	while (true)
	{
		std::unique_lock<std::mutex> lck(mtx);
		cv.wait(lck);
		int msgCode;
		if (!_queRcvMessages.empty())
		{
			RecievedMessage * msg = _queRcvMessages.front();
			_queRcvMessages.pop();
			msgCode = msg->getMessageCode();
			try
			{
				if (SIGN_IN_REQ == msgCode)//200
				{
					handleSignin(msg);
				}
				else if (SIGN_OUT_REQ == msgCode)//201
				{
					handleSignout(msg);
				}
				else if (SIGN_UP_REQ == msgCode)//203
				{
					handleSignup(msg);
				}
				else if (ROOM_LIST_REQ == msgCode)//205
				{
					handleGetRooms(msg);
				}
				else if (USER_ROOM_LIST_REQ == msgCode)//207
				{
					handleGetUsersInRoom(msg);
				}
				else if (JOIN_ROOM_REQ == msgCode)//209
				{
					handleJoinRoom(msg);
				}
				else if (LEAVE_ROOM_REQ == msgCode)//211
				{
					handleLeaveRoom(msg);
				}
				else if (CREATE_ROOM_REQ == msgCode)//213
				{
					handleCreateRoom(msg);
				}
				else if (CLOSE_ROOM_REQ == msgCode)//215
				{
					handleCloseRoom(msg);
				}
				else if (BEGIN_GAME_REQ == msgCode)//217
				{
					handleStartGame(msg);
				}
				else if (CLIENT_ANS == msgCode)//219
				{
					handlePlayerAnswer(msg);
				}
				else if (LEAVE_GAME_MSG == msgCode)//222
				{
					handleLeaveGame(msg);
				}
				else if (BEST_SOCORES_REQ == msgCode)
				{
					handleBestScores(msg);
				}
				else if (MY_STATUS_REQ == msgCode)
				{
					handleGetPersonalStatus(msg);
				}
				else if (LEAVE_APP == msgCode)
				{
					safeDeleteUser(msg); //299
				}
				else
				{
					safeDeleteUser(msg); 
				}

			}
			catch (...)
			{
				safeDeleteUser(msg);
			}
			
		}
	}
	
}

void TriviaServer::safeDeleteUser(RecievedMessage* msg)
{
	try
	{
		handleSignout(buildRecieveMessage(msg->getSocket(), SIGN_OUT_REQ));
		if (closesocket(msg->getSocket()))
		{
			exit(1);
		}
	}
	catch (...) {}
}
User* TriviaServer::handleSignin(RecievedMessage* msg)//need to look on this
{
	string userName = msg->getValues()[0];
	string pass = msg->getValues()[1];
	map<string, string> ::iterator it = _listOfUsers.begin();

	while (it != _listOfUsers.end())
	{
		if (it->first == userName && it->second == pass)
		{

			User* user = new User(userName, msg->getSocket());
			_connectedUsers.insert(std::pair<SOCKET, User*>(msg->getSocket(), user));
			Helper::sendData(msg->getSocket(),SIGN_IN_ANS_SUCCESS);
			return user;
		}
		++it;
	}
	Helper::sendData(msg->getSocket(),SIGN_IN_ANS_WORNG_DETAILS);
	return NULL;
	/**
	if (db.isUserAndPassMatch(userName,pass))
	{
	if (getUserByName(userName))
	{
	send(_s, SIGN_IN_ANS_USER_IS_ALREADY_CONNECTED, 50, 0);
	}
	else
	{
	User* user = new User(userName,_s);
	_connectedUsers.insert(std::pair<SOCKET, User*>(_s, user));
	send(_s, SIGN_IN_ANS_SUCCESS, 50, 0);
	return user;
	}
	}
	else
	{
	send(_s, SIGN_IN_ANS_WORNG_DETAILS	, 50, 0);
	}
	return NULL;
	*/
}//200R
void TriviaServer::handleSignout(RecievedMessage* msg)
{
	if (msg->getUser())
	{
		std::map<SOCKET, User*>::iterator it;

		it = _connectedUsers.find(msg->getSocket());
		if (it != _connectedUsers.end())
		{
			_connectedUsers.erase(it);
		}
		if (msg->getUser()->getRoom())
		{
			if (!handleCloseRoom(msg))
			{
				handleLeaveRoom(msg);
			}
		}
		else if (msg->getUser()->getGame())
		{
			handleLeaveGame(msg);
		}		
	}

}//201
bool TriviaServer::handleSignup(RecievedMessage* msg)
{
	map<string, string>::iterator it = _listOfUsers.begin();
	string userName = msg->getValues()[0];
	string pass = msg->getValues()[1];
	int count = 0;
	if (v.isPasswordValid(pass))
	{
		if (v.isUserNameValid(userName))
		{
			while (it != _listOfUsers.end())
			{
				if (it->first == userName)
				{
					Helper::sendData(msg->getSocket(),SIGN_UP_ANS_USERNAME_IS_ALREADY_EXIST);
					return false;
				}
				it++;
			}
			_listOfUsers[userName] = pass;
			Helper::sendData(msg->getSocket(), SIGN_UP_ANS_SUCCESS);
			return true;
			//if (db.addNewUser(userName, msg->getPass(), "lalal"))//
			//{
			//_listOfUsers[userName] = msg->getValues()[1];
			//}
			//else
			//{
			//	send(_s, SIGN_UP_ANS_OTHER, 50, 0);
			//	return false;
			//}

		}
		else
		{
			Helper::sendData(msg->getSocket(), SIGN_UP_ANS_USERNAME_IS_ILLEGAL);
			return false;
		}
	}
	else
	{
		Helper::sendData(msg->getSocket(), SIGN_UP_ANS_PASS_ILLEGAL);
		return false;
	}//203
	return false;
}

void TriviaServer::handleLeaveGame(RecievedMessage* msg)
{
	if (msg->getUser()->leaveGame())
	{
		msg->getUser()->getGame()->~Game();
	}
}//222
void TriviaServer::handleStartGame(RecievedMessage* msg)
{
	try
	{
		Game *game = new Game(msg->getUser()->getRoom()->getUsers(), msg->getUser()->getRoom()->getQuestionsNo(), db, msg->getUser()->getRoom()->getId());
		std::map<int, Room*> ::iterator it;
		
		it = this->_roomsList.find(game->getID());
		if (it != this->_roomsList.end())
		{
			this->_roomsList.erase(it);
		}

		game->sendFirstQuestion();
	}
	catch (...)
	{
		msg->getUser()->send(BEGIN_GAME_FAIL_ANS);
	}
}//217
void TriviaServer::handlePlayerAnswer(RecievedMessage* msg)
{
	if (msg->getUser()->getGame() != nullptr)
	{
		bool ok = msg->getUser()->getGame()->handleAnswerFromUser(msg->getUser(), atoi(msg->getValues()[0].c_str()), atoi(msg->getValues()[1].c_str()));

		if (!ok)
		{
			delete msg->getUser()->getGame();
		}
	}
}//219
bool TriviaServer::handleCreateRoom(RecievedMessage* msg)
{
	if (msg->getUser())
	{
		string roomName;
		int playersNum, questionsNum, questionTimeInSec;
		roomName = msg->getValues()[0];
		playersNum = atoi(msg->getValues()[1].c_str());
		questionsNum = atoi(msg->getValues()[2].c_str());
		questionTimeInSec = atoi(msg->getValues()[3].c_str());

		bool success = msg->getUser()->createRoom(_roomIdSequence, roomName, playersNum, questionsNum, questionTimeInSec);
		if (success)
		{
			_roomsList[_roomIdSequence] = msg->getUser()->getRoom();
			_roomIdSequence++;
			return true;
		}
	}

	return false;
}//213
bool TriviaServer::handleCloseRoom(RecievedMessage* msg)
{
	if (msg->getUser()->getRoom())
	{
		int res = msg->getUser()->closeRoom();
		if (res == -1)
		{
			return false;
		}
		else
		{
			std::map<int, Room*>::iterator it;
			it = _roomsList.find(res);
			_roomsList.erase(it);
			
			return true;
		}
	}
	return false;
}//215
bool TriviaServer::handleJoinRoom(RecievedMessage* msg)
{
	string msgg;
	if (msg->getUser())
	{
		int idRoom = atoi(msg->getValues()[0].c_str());
		if (getRoomById(idRoom))
		{
			return (msg->getUser()->joinRoom(getRoomById(idRoom)));
		}
		else
		{
			msg->getUser()->send(JOIN_ROOM__FAIL_NOT_EXIST_ANS);
			return false;
		}
	}
	return false;
 
}//209

bool TriviaServer::handleLeaveRoom(RecievedMessage* msg)
{
	if (msg->getUser())
	{
		if (msg->getUser()->getRoom())
		{
			msg->getUser()->leaveRoom();
			return true;
		}
		else
		{
			return false;
		}
	}
	return false;
}//211

void TriviaServer::handleGetUsersInRoom(RecievedMessage* msg)
{
	if (msg->getUser())
	{
		int idRoom = atoi(msg->getValues()[0].c_str());
		if (getRoomById(idRoom))
		{
			msg->getUser()->send(getRoomById(idRoom)->getUsersListMessage());
		}
	}

}//207
void TriviaServer::handleGetRooms(RecievedMessage* msg)
{
	ostringstream convert;
	convert << _roomsList.size();
	string str = ROOM_LIST_ANS;
	if (_roomsList.size() < 10)
	{
		str += "000";
		str += convert.str();
	}
	else if (_roomsList.size() <= 10 && _roomsList.size() < 100)
	{
		str += "00";
		str += convert.str();
	}
	else if (_roomsList.size() <= 100 && _roomsList.size() < 1000)
	{
		str += "0";
		str += convert.str();
	}
	else
	{
		str += convert.str();
	}

	if (_roomsList.size() != 0)
	{
		std::map<int, Room*>::iterator it = _roomsList.begin();

		while (it != _roomsList.end())
		{
			int size = it->first;

			ostringstream convert2;
			convert2 << size;
			str += convert2.str();
			
			ostringstream convert3;

			convert3 << it->second->getName().size();

			if (it->second->getName().size() < 10)
			{
				str += "0";
				str += convert3.str();
			}
			else
			{
				str += convert3.str();
			}
			str += it->second->getName();

			it++;
		}
	}

	msg->getUser()->send(str);

}//205
void TriviaServer::handleBestScores(RecievedMessage* msg){}//223
void TriviaServer::handleGetPersonalStatus(RecievedMessage* msg){}//225

void TriviaServer::addRecievedMessage(RecievedMessage* msg)
{
	unique_lock<std::mutex> lock(mtx_queRcvMessages);
	_queRcvMessages.push(msg);
	lock.unlock();
	cv.notify_one();

}//225
RecievedMessage* TriviaServer::buildRecieveMessage(SOCKET client_socket, int msgCode)
{
	int len;
	RecievedMessage* msg = NULL;
	vector<string> vec;

	switch (msgCode)
	{
	case SIGN_IN_REQ:
		len = Helper::getIntPartFromSocket(client_socket, 2);
		vec.push_back(Helper::getStringPartFromSocket(client_socket, len));
		len = Helper::getIntPartFromSocket(client_socket, 2);
		vec.push_back(Helper::getStringPartFromSocket(client_socket, len));
		msg = new RecievedMessage(client_socket, getUserBySocket(client_socket), msgCode, vec);
		return msg;
		break;
	case SIGN_OUT_REQ:
		msg = new RecievedMessage(client_socket, getUserBySocket(client_socket), msgCode, vec);
		return msg;
		break;
	case SIGN_UP_REQ:
		len = Helper::getIntPartFromSocket(client_socket, 2);
		vec.push_back(Helper::getStringPartFromSocket(client_socket, len));
		len = Helper::getIntPartFromSocket(client_socket, 2);
		vec.push_back(Helper::getStringPartFromSocket(client_socket, len));
		len = Helper::getIntPartFromSocket(client_socket, 2);
		vec.push_back(Helper::getStringPartFromSocket(client_socket, len));
		msg = new RecievedMessage(client_socket, getUserBySocket(client_socket), msgCode, vec);
		return msg;
		break;
	case ROOM_LIST_REQ:
		msg = new RecievedMessage(client_socket, getUserBySocket(client_socket), msgCode, vec);
		return msg;
		break;
	case USER_ROOM_LIST_REQ:
		vec.push_back(Helper::getStringPartFromSocket(client_socket, 4));
		msg = new RecievedMessage(client_socket, getUserBySocket(client_socket), msgCode, vec);
		return msg;
		break;
	case JOIN_ROOM_REQ:
		vec.push_back(Helper::getStringPartFromSocket(client_socket, 4));
		msg = new RecievedMessage(client_socket, getUserBySocket(client_socket), msgCode, vec);
		return msg;
		break;
	case LEAVE_ROOM_REQ:
		msg = new RecievedMessage(client_socket, getUserBySocket(client_socket), msgCode, vec);
		return msg;
		break;
	case CREATE_ROOM_REQ:
		len = Helper::getIntPartFromSocket(client_socket, 2);
		vec.push_back(Helper::getStringPartFromSocket(client_socket, len));
		vec.push_back(Helper::getStringPartFromSocket(client_socket, 1));
		vec.push_back(Helper::getStringPartFromSocket(client_socket, 2));
		vec.push_back(Helper::getStringPartFromSocket(client_socket, 2));
		msg = new RecievedMessage(client_socket, getUserBySocket(client_socket), msgCode, vec);
		return msg;
		break;
	case CLOSE_ROOM_REQ:
		msg = new RecievedMessage(client_socket, getUserBySocket(client_socket), msgCode, vec);
		return msg;
		break;
	case BEGIN_GAME_REQ:
		msg = new RecievedMessage(client_socket, getUserBySocket(client_socket), msgCode, vec);
		return msg;
		break;
	case CLIENT_ANS:
		vec.push_back(Helper::getStringPartFromSocket(client_socket, 1));
		vec.push_back(Helper::getStringPartFromSocket(client_socket, 2));
		msg = new RecievedMessage(client_socket, getUserBySocket(client_socket), msgCode, vec);
		return msg;
		break;
	case LEAVE_APP:
		msg = new RecievedMessage(client_socket, getUserBySocket(client_socket), msgCode, vec);
		return msg;
		break;
	default:
		msg = new RecievedMessage(client_socket, getUserBySocket(client_socket), msgCode, vec);
		return msg;
		break;
	}
	msg = new RecievedMessage(client_socket, getUserBySocket(client_socket), msgCode, vec);
	return msg;
}

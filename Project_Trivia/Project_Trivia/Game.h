#pragma once
#include <iostream>
#include <map>
#include "DataBase.h"
#include "User.h"
#include <fstream>
#include <string>


using namespace std;
class User;
class Game
{
public:
	Game(const vector<User*>& players, int questionsNo, DataBase& db, int id);
	~Game();

	void sendQuestionToAllUsers();
	void handleFinishGame();
	void sendFirstQuestion();
	bool handleNextTurn();
	bool handleAnswerFromUser(User* user, int answerNo, int time);
	bool leaveGame(User* currUser);
	int getID();

private:
	map<std::string, int> _results;
	int _currentTurnAnswers;
	vector<Question*> _questions;
	vector<User*> _players;
	int _questions_no;
	int _currQuestionIndex;
	DataBase _db;
	int _id;

};
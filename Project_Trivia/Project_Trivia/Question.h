#pragma once
#include <iostream>
#include <string>
#include <stdio.h>      
#include <stdlib.h>  
#include <time.h>   

using namespace std;

class Question
{
public:
	Question(int id, string question, string correctAnswer, string answer2, string answer3, string answer4);

	string getQuestion();
	string* getAnswers();
	int getCorrectAnsewrIndex();
	int getID();

private:
	string _question;
	string _answers[4];
	int _correctAnswerIndex;
	int _id;
};


#include "DataBase.h"

static int callback(void *NotUsed, int argc, char **argv, char **azColName){
	/**
	int i;
	for (i = 0; i<argc; i++){
		printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
	}
	printf("\n");
	return 0*/
	return 0;
}
DataBase::DataBase()
{	
	/**
	try
	{
		int rc;
		rc = sqlite3_open("Trivia.db", &_db);
		if (rc){
			fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(_db));
		}
		else{
			fprintf(stdout, "Opened database successfully\n");
		}

		sql = "CREATE TABLE t_users("  \
			"USER_NAME	TEXT	PRIMARY_KEY	NOT NULL," \
			"PASSWORD	  TEXT   NOT NULL," \
			"EMAIL        TEXT   NOT NULL);";
		// Execute SQL statement 
		rc = sqlite3_exec(_db, sql, callback, 0, &zErrMsg);
		if (rc != SQLITE_OK){
			fprintf(stderr, "SQL error: %s\n", zErrMsg);
			sqlite3_free(zErrMsg);
		}
		else{
			fprintf(stdout, "Table created successfully\n");
		}

		sql = "CREATE TABLE t_games("  \
			"GAME_ID	INTEGER	PRIMARY_KEY   AUTOINCREMENT," \
			"STATUS		INT		NOT NULL," \
			"DATETIME_START_TIME	TEXT	NOT NULL,"\
			"DATETIME_END_TIME		TEXT	NOT NULL);";
		// Execute SQL statement 
		rc = sqlite3_exec(_db, sql, callback, 0, &zErrMsg);
		if (rc != SQLITE_OK){
			fprintf(stderr, "SQL error: %s\n", zErrMsg);
			sqlite3_free(zErrMsg);
		}
		else{
			fprintf(stdout, "Table created successfully\n");
		}

		sql = "CREATE TABLE  t_questions("  \
			"QUESTUON_ID	INTEGER PRIMARY_KEY   AUTOINCREMENT," \
			"QUESTION	TEXT	NOT NULL," \
			"CORRECT_ANS	TEXT	NOT NULL,"
			"ANS2	TEXT	NOT NULL,"
			"ANS3	TEXT	NOT NULL,"
			"ANS4	TEXT	NOT NULL); ";
		// Execute SQL statement 
		rc = sqlite3_exec(_db, sql, callback, 0, &zErrMsg);
		if (rc != SQLITE_OK){
			fprintf(stderr, "SQL error: %s\n", zErrMsg);
			sqlite3_free(zErrMsg);
		}
		else{
			fprintf(stdout, "Table created successfully\n");
		}
		sql = "CREATE TABLE t_players_answers("\
			"GAME_ID	TEXT	NOT NULL, "\
			"USER_NAME	TEXT	NOT NULL,"\
			"QUESTION_ID	TEXT	NOT NULL,"\
			"PRIMARY KEY CLUSTERED (GAME_ID, USER_NAME, QUESTION_ID),"\
			"FOREIGN KEY(GAME_ID) REFERENCES[t.games](GAME_ID) ON UPDATE  NO ACTION  ON DELETE  CASCADE,"\
			"FOREIGN KEY(USER_NAME) REFERENCES[t.users](USER_NAME) ON UPDATE  NO ACTION  ON DELETE  CASCADE,"\
			"FOREIGN KEY(QUESTUON_ID) REFERENCES[t.questions](QUESTUON_ID) ON UPDATE  NO ACTION  ON DELETE  CASCADE,"\
			"PLAYER_ANSWER		TEXT	NOT NULL," \
			"IS_CORRECT		INT		NOT NULL,"\
			"ANSWER_TIME	INT		NOT NULL);";
		// Execute SQL statement 
		rc = sqlite3_exec(_db, sql, callback, 0, &zErrMsg);
		if (rc != SQLITE_OK){
			fprintf(stderr, "SQL error: %s\n", zErrMsg);
			sqlite3_free(zErrMsg);
		}
		else{
			fprintf(stdout, "Table created successfully\n");
		}

	}
	catch(...)
	{
		std::cout << "Unknown exception in open DB" << std::endl;
	}	
	*/
}
DataBase::~DataBase()
{
	/**
	sqlite3_close(_db);
	*/
}

bool DataBase::isUserExits(string username)
{
	/**
	int rc;
	sql = "Select * from t_users where USER_NAME = username";
	rc = sqlite3_exec(_db, sql, callback, 0, &zErrMsg);
	if (rc == SQLITE_OK)
	{
		return true;
	}
	return false;
	*/
	return true;
}
bool DataBase::addNewUser(string username, string password, string email)
{  
	/**
	int rc;
	sql = "INSERT INTO t.users (USER_NAME, PASSWORD, EMAIL) " \
		"VALUES (username, password, email);";
	rc = sqlite3_exec(_db, sql, callback, 0, &zErrMsg);
	if (rc == SQLITE_OK)
	{
		return true;
	}
	return false;
	*/
	return true;
}
bool DataBase::isUserAndPassMatch(string username, string password)
{
	/**
	int rc;
	sql = "Select * from t_users where USER_NAME = username and PASSWORD = password";
	rc = sqlite3_exec(_db, sql, callback, 0, &zErrMsg);
	if (rc == SQLITE_OK)
	{
		return true;
	}
	return false;
	*/
	return true;
}

vector<Question*> DataBase::initQuestions(int questionsNo)
{
	return _vec;
}

int DataBase::insertNewGame()
{
	/*
	int rc;
	time_t  timev;
	sql = "INSERT INTO t_games (GAME_ID, STATUS, DATETIME_START_TIME, DATETIME_END_TIME) " \
		"VALUES (0, time(&timev), null);";
	rc = sqlite3_exec(_db, sql, callback, 0, &zErrMsg);
	if (rc == SQLITE_OK)
	{
		return true;//need return the id game
	}
	return false;
	*/
	return true;
}
bool DataBase::updateGameStatus(int gameId)
{
	/**
	int rc;
	time_t  timev;
	sql = "UPDATE t_games set STATUS = 1 where GAME_ID=gameId; " \
		  "UPDATE t_games set DATETIME_END_TIME =time(&timev)  where GAME_ID=gameId";
	rc = sqlite3_exec(_db, sql, callback, 0, &zErrMsg);
	if (rc == SQLITE_OK)
	{
		return true;
	}
	return false;
	*/
	return true;
}
bool DataBase::addAnswerToPlayer(int gameId, string username, int questionId, string answer, bool isCorrect, int answerTime)
{
	/*
	int rc;
	time_t  timev;
	sql = "INSERT INTO t_players_answers(GAME_ID, USER_NAME, QUESTION_ID), ANSWER, IS_CORRECT, ANSWER_TIME " \
		"VALUES (gameId, username, questionId, answer, isCorrect, answerTime);";
	rc = sqlite3_exec(_db, sql, callback, 0, &zErrMsg);
	if (rc == SQLITE_OK)
	{
		return true;
	}
	return false;
	*/
	return true;
}
#pragma once
#include <string>
#include "Helper.h"
#include "Game.h"
#include "Room.h"

using namespace std;

class Room;
class Game;
class User
{
public:
	User(string username, SOCKET sock);

	void send(string message);
	void setGame(Game* gm);
	void clearRoom();
	bool createRoom(int roomId, string roomName, int maxUsers, int questionsNum, int questionTime);
	bool joinRoom(Room* newRoom);
	void leaveRoom();
	int closeRoom();
	bool leaveGame();
	string getUserName();
	SOCKET getSocket();
	Room* getRoom();
	Game* getGame();


private:
	string _userName;
	Room* _currRoom;
	Game* _currGame;
	SOCKET _sock;
};


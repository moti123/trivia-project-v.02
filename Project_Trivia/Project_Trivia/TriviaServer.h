#include <iostream>
#include <string>
#include <thread>
#include <map>
#include <queue>
#include <mutex>
#include <condition_variable>
#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#pragma comment(lib, "Ws2_32.lib")

#include "RecievedMessage.h"
#include "Helper.h"
#include "User.h"
#include "Room.h"
#include "Validator.h"
#include "DataBase.h"


using namespace std;

static const char* PORT = "8820";
static const unsigned int IFACE = 0;
static Helper helper;
static DataBase db;
static Validator v;
static int _roomIdSequence = 1000;
static mutex mtx_queRcvMessages;
static mutex mtx;
static RecievedMessage * msg = NULL;


class TriviaServer
{
public:
	TriviaServer();
	~TriviaServer();

	void serve();
	void bindAndListen();
	void accept();

	void clientHandler(SOCKET client_socket);

	Room* getRoomById(int roomId);
	User* getUserByName(string userName);
	User* getUserBySocket(SOCKET client_socket);

	void handleRecievedMessages();
	void safeDeleteUser(RecievedMessage* msg);
	User* handleSignin(RecievedMessage* msg);
	void handleSignout(RecievedMessage* msg);
	bool handleSignup(RecievedMessage* msg);
	void handleLeaveGame(RecievedMessage* msg);//222
	void handleStartGame(RecievedMessage* msg);//217
	void handlePlayerAnswer(RecievedMessage* msg);//219
	bool handleCreateRoom(RecievedMessage* msg);//213
	bool handleCloseRoom(RecievedMessage* msg);//215
	bool handleJoinRoom(RecievedMessage* msg);//209
	bool handleLeaveRoom(RecievedMessage* msg);//211
	void handleGetUsersInRoom(RecievedMessage* msg);//207
	void handleGetRooms(RecievedMessage* msg);//205
	void handleBestScores(RecievedMessage* msg);//223
	void handleGetPersonalStatus(RecievedMessage* msg);//225
	void addRecievedMessage(RecievedMessage* msg);//225
	RecievedMessage* buildRecieveMessage(SOCKET client_socket, int msgCode);//225

private:
	std::map<SOCKET, User*> _connectedUsers;
	
	queue<RecievedMessage*> _queRcvMessages;
	SOCKET _s;
	std::map<int, Room*> _roomsList;
	std::map<string, string> _listOfUsers;
	condition_variable cv;

};